module gitlab.com/demonihin/NetTest/nettest

go 1.12

require (
	github.com/aeden/traceroute v0.0.0-20181124220833-147686d9cb0f // indirect
	github.com/kanocz/tracelib v0.0.0-20180504160550-77515e18754a
	github.com/pkg/errors v0.8.1
	github.com/rapid7/go-get-proxied v0.0.0-20181210221417-16249a544090
	github.com/sparrc/go-ping v0.0.0-20181106165434-ef3ab45e41b0
	github.com/stretchr/testify v1.3.0 // indirect
	golang.org/x/net v0.0.0-20190404232315-eb5bcb51f2a3 // indirect
)
