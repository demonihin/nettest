package nettest

import (
	"fmt"
	"net"
	"sync"
)

// LookupHostResult - Result of one DNS Lookup
type LookupHostResult struct {
	ResolvedAddresses []string `yaml:"ResolvedAddresses,omitempty"`
	HostnameToResolve string   `yaml:"HostnameToResolve,omitempty"`
	Error             string   `yaml:"Error,omitempty"`
}

// LookupHostTask - One Test task
type LookupHostTask struct {
	HostnameToResolve string
}

// AggregateLookupHostParallel - performs LookupHostParallel, grabs results
// and returns them as slice
func AggregateLookupHostParallel(tasks []LookupHostTask,
	parallelTasks int, printProgress bool) []LookupHostResult {

	var res = make([]LookupHostResult, 0, len(tasks))

	for r := range LookupHostParallel(tasks, parallelTasks,
		printProgress) {
		res = append(res, r)
	}

	return res
}

// LookupHostParallel - Performs parallel DNS lookup and returns result
func LookupHostParallel(tasks []LookupHostTask,
	parallelTasks int, printProgress bool) <-chan LookupHostResult {
	// Create sender
	var tasksGen = lookupHostTasksSender(tasks)

	// Create workers
	var workersOut = make([]<-chan LookupHostResult, parallelTasks)
	for i := 0; i < parallelTasks; i++ {
		workersOut[i] = lookupHostTasksProcessor(tasksGen, printProgress)
	}

	// Grab result
	return lookupHostResultGrabber(workersOut)
}

// lookupHostTasksSender - Sends DNS lookup tasks to workers
func lookupHostTasksSender(tasks []LookupHostTask) <-chan LookupHostTask {
	var out = make(chan LookupHostTask)
	go func() {
		for _, task := range tasks {
			out <- task
		}

		close(out)
	}()

	return out
}

// lookupHostTasksProcessor - Processes DNS Lookup and returns results
func lookupHostTasksProcessor(
	inChan <-chan LookupHostTask, printProgress bool) <-chan LookupHostResult {
	var out = make(chan LookupHostResult)

	go func() {
		for inTask := range inChan {
			// Process
			hosts, err := net.LookupHost(inTask.HostnameToResolve)

			if err != nil {
				out <- LookupHostResult{
					Error: err.Error(),
				}
			} else {
				out <- LookupHostResult{
					ResolvedAddresses: hosts,
					HostnameToResolve: inTask.HostnameToResolve,
				}
			}
			if printProgress {
				if err == nil {
					println(fmt.Sprintf(
						"Succeeded DNS Lookup for host %v. Addresses: %v",
						inTask.HostnameToResolve, hosts))
				} else {
					println(fmt.Sprintf(
						"Error DNS Lookup for host %v. Error: %v",
						inTask.HostnameToResolve, err))
				}
			}
		}

		close(out)
	}()

	return out
}

// lookupHostResultGrabber - Gets results
func lookupHostResultGrabber(
	resultChannels []<-chan LookupHostResult) <-chan LookupHostResult {

	var out = make(chan LookupHostResult)
	var waitGroup sync.WaitGroup

	// Start merge tasks
	// Create a function which gets results from one task and sends to another
	for _, inResChan := range resultChannels {
		waitGroup.Add(1)

		go func(ch <-chan LookupHostResult) {
			for val := range ch {
				out <- val
			}

			// Task finished
			waitGroup.Done()
		}(inResChan)
	}

	// Start result waiter
	go func() {
		waitGroup.Wait()
		close(out)
	}()

	return out
}
