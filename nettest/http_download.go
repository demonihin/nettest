package nettest

import (
	"fmt"
	"net/http"
	"net/url"
	"sync"

	"github.com/rapid7/go-get-proxied/proxy"
)

// GetHTTPResult - Result of one HTTP task
type GetHTTPResult struct {
	RequestedURL string `yaml:"RequestedURL,omitempty"`
	StatusCode   int    `yaml:"StatusCode,omitempty"`
	Status       string `yaml:"Status,omitempty"`
	ProxyURL     string `yaml:"ProxyURL,omitempty"`
	ProxyUsed    bool   `yaml:"ProxyUsed"`
	Error        string `yaml:"Error,omitempty"`
}

// GetHTTPTask - One HTTP GET test task
type GetHTTPTask struct {
	URL         string
	DetectProxy bool
}

// AggregateGetHTTPParallel - performs GetHTTPParallel, grabs results
// and returns them as slice
func AggregateGetHTTPParallel(tasks []GetHTTPTask,
	parallelTasks int, printProgress bool) []GetHTTPResult {

	var res = make([]GetHTTPResult, 0, len(tasks))

	for r := range GetHTTPParallel(tasks, parallelTasks, printProgress) {
		res = append(res, r)
	}

	return res
}

// GetHTTPParallel - Performs parallel HTTP GETs and returns result
func GetHTTPParallel(tasks []GetHTTPTask,
	parallelTasks int, printProgress bool) <-chan GetHTTPResult {

	// Create sender
	var tasksGen = getHTTPTasksSender(tasks)

	// Create workers
	var workersOut = make([]<-chan GetHTTPResult, parallelTasks)
	for i := 0; i < parallelTasks; i++ {
		workersOut[i] = getHTTPTasksProcessor(tasksGen, printProgress)
	}

	// Grab result
	return getHTTPResultGrabber(workersOut)
}

// getHTTPTasksSender - Sends HTTP GET tasks to workers
func getHTTPTasksSender(tasks []GetHTTPTask) <-chan GetHTTPTask {

	var out = make(chan GetHTTPTask)
	go func() {
		for _, task := range tasks {
			out <- task
		}

		close(out)
	}()

	return out
}

// getHTTPTasksProcessor - Processes GETs and returns results
func getHTTPTasksProcessor(inChan <-chan GetHTTPTask,
	printProgress bool) <-chan GetHTTPResult {

	var out = make(chan GetHTTPResult)

	go func() {
		for inTask := range inChan {
			// Process
			transport := &http.Transport{}

			// Detect proxy if asked to
			if inTask.DetectProxy {
				transport.Proxy = getProxy
			}

			// Create client and perform connection
			httpClient := &http.Client{
				Transport: transport,
			}

			resp, err := httpClient.Get(inTask.URL)

			// Build result
			if err != nil {
				out <- GetHTTPResult{
					RequestedURL: inTask.URL,
					Error:        err.Error(),
				}

				// print error messages
				if printProgress {
					println(fmt.Sprintf(
						"Error HTTP Get for host %v. Error message: %v",
						inTask.URL, err.Error()))
				}
			} else {
				// Find out if proxy was used
				// I'm not sure about this part
				// ToDo: test it and possibly rewrite
				var (
					proxyUsed bool
					proxyURL  string
				)

				if inTask.DetectProxy {
					// Lookup for proxy using a URL from the request
					proxy, err := getProxy(resp.Request)
					if err != nil {
						out <- GetHTTPResult{
							RequestedURL: inTask.URL,
							Error:        err.Error(),
						}
					}
					proxyUsed = proxy != nil
					if proxyUsed {
						proxyURL = proxy.String()
					}
				}

				out <- GetHTTPResult{
					RequestedURL: inTask.URL,
					StatusCode:   resp.StatusCode,
					Status:       resp.Status,
					ProxyURL:     proxyURL,
					ProxyUsed:    proxyUsed,
				}

				// print progress messages
				if printProgress {
					println(fmt.Sprintf(
						"Succeeded HTTP Get for host %v. Status: %v. Used proxy: %v. Proxy: %v",
						inTask.URL, resp.Status, proxyUsed, proxyURL))
				}

			}
		}

		close(out)
	}()

	return out
}

// getHTTPResultGrabber - Gets results
func getHTTPResultGrabber(
	resultChannels []<-chan GetHTTPResult) <-chan GetHTTPResult {

	var out = make(chan GetHTTPResult)
	var waitGroup sync.WaitGroup

	// Start merge tasks
	// Create a function which gets results from one task and sends to another
	for _, inResChan := range resultChannels {
		waitGroup.Add(1)

		go func(ch <-chan GetHTTPResult) {
			for val := range ch {
				out <- val
			}

			// Task finished
			waitGroup.Done()
		}(inResChan)
	}

	// Start result waiter
	go func() {
		waitGroup.Wait()
		close(out)
	}()

	return out
}

// getProxy - Returns proxy for URL
func getProxy(r *http.Request) (*url.URL, error) {
	var proxyProvider = proxy.NewProvider("")
	proxy := proxyProvider.GetProxy(r.URL.Scheme, r.URL.String())

	if proxy == nil {
		return nil, nil
	}
	return proxy.URL(), nil
}
