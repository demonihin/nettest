package nettest

import (
	"fmt"
	"sync"
	"time"

	sparrc "github.com/sparrc/go-ping"
)

const (
	// pingTimeoutMultiplifier - is a coefficient which contains
	// value to add extra time to receive all sent packets
	pingTimeoutMultiplifier float32 = 1.5
)

// PingResult - Contains information about result of one Ping task
type PingResult struct {
	Statistics       sparrc.Statistics `yaml:"Statistics,omitempty"`
	PingedHost       string            `yaml:"PingedHost,omitempty"`
	TotalPackets     int               `yaml:"TotalPackets,omitempty"`
	SucceededPackets int               `yaml:"SucceededPackets,omitempty"`
	Error            string            `yaml:"Error,omitempty"`
}

// PingTask - One task to ping One host
type PingTask struct {
	Destination  string
	PacketsCount int
	PacketSize   int
	IntervalMS   int // Interval between packets in milliseconds
}

// AggregatePingParallel - performs PingParallel, grabs results
// and returns them as slice
func AggregatePingParallel(tasks []PingTask,
	parallelTasks int, printProgress bool) []PingResult {

	var res = make([]PingResult, 0, len(tasks))

	for r := range PingParallel(tasks, parallelTasks, printProgress) {
		res = append(res, r)
	}

	return res
}

// PingParallel - perform ping in parallel threads
func PingParallel(tasks []PingTask, parallelTasks int,
	printProgress bool) <-chan PingResult {
	// Create sender
	var tasksGen = pingTasksSender(tasks)

	// Create workers
	var workersOut = make([]<-chan PingResult, parallelTasks)
	for i := 0; i < parallelTasks; i++ {
		workersOut[i] = pingTasksProcessor(tasksGen, printProgress)
	}

	// Grab result
	return pingResultGrabber(workersOut)
}

func pingTasksSender(tasks []PingTask) <-chan PingTask {
	var out = make(chan PingTask)
	go func() {
		for _, task := range tasks {
			out <- task
		}

		close(out)
	}()

	return out
}

func pingTasksProcessor(inChan <-chan PingTask, printProgress bool) <-chan PingResult {
	var out = make(chan PingResult)

	go func() {
		for inTask := range inChan {
			// Process
			var pinger, err = sparrc.NewPinger(inTask.Destination)
			if err != nil {
				out <- PingResult{Error: err.Error()}

				// Skip remained steps
				continue
			}

			// Set parameters
			pinger.Count = inTask.PacketsCount
			pinger.Size = inTask.PacketSize
			pinger.Interval = time.Duration(
				inTask.IntervalMS) * time.Millisecond

			// Timeout set as packet interval + extra time coefficient
			var ptm = pinger.Interval * time.Duration(
				float32(pinger.Count)*pingTimeoutMultiplifier)
			pinger.Timeout = ptm

			// Set privileged - Use ICMP ping
			pinger.SetPrivileged(true)

			// Execute
			pinger.Run()

			var pingResults = pinger.Statistics()

			// Error check using side-effect:
			// sent packets count in Statistics must be the same as
			// inTask.PacketsCount
			var pingErrText string
			if !(pingResults.PacketsSent == inTask.PacketsCount &&
				pingResults.PacketsRecv == inTask.PacketsCount) {
				pingErrText = fmt.Sprintf(
					"Error executing ping: Sent only %v from %v packets",
					pingResults.PacketsSent, inTask.PacketsCount)
			}

			out <- PingResult{
				Statistics:       *pingResults,
				Error:            pingErrText,
				PingedHost:       inTask.Destination,
				TotalPackets:     inTask.PacketsCount,
				SucceededPackets: pingResults.PacketsRecv,
			}

			if printProgress {
				if pingErrText == "" {
					println(fmt.Sprintf(
						"Succeeded ping to host %v. Address: %v. rtt min/avg/max = %v/%v/%v ms",
						inTask.Destination, pingResults.Addr, pingResults.MinRtt,
						pingResults.AvgRtt, pingResults.MaxRtt))
				} else {
					println(fmt.Sprintf(
						"Error ping to host %v. Address: %v. %v ",
						inTask.Destination, pingResults.Addr, pingErrText))
				}
			}

		}

		close(out)
	}()

	return out
}

func pingResultGrabber(resultChannels []<-chan PingResult) <-chan PingResult {
	var out = make(chan PingResult)
	var waitGroup sync.WaitGroup

	// Start merge tasks
	// Create a function which gets results from one task and sends to another
	for _, inResChan := range resultChannels {
		waitGroup.Add(1)

		go func(ch <-chan PingResult) {
			for val := range ch {
				out <- val
			}

			// Task finished
			waitGroup.Done()
		}(inResChan)
	}

	// Start result waiter
	go func() {
		waitGroup.Wait()
		close(out)
	}()

	return out
}
