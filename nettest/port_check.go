package nettest

import (
	"fmt"
	"net"
	"sync"
)

const (
	// DialProtocolTCP - Use TCP protocol
	DialProtocolTCP = "tcp"
	// DialProtocolUDP - Use UDP protocol
	DialProtocolUDP = "udp"

	// DialProtocolTCP4 = "tcp4"
	// DialProtocolTCP6 = "tcp6"

	// DialProtocolUDP4 = "udp4"
	// DialProtocolUDP6 = "udp6"
)

// DialHostPortTask - One task to check if a port is opened
type DialHostPortTask struct {
	// TCP or UDP
	Protocol string

	// Hostname + Port
	HostnamePort string
}

// DialHostPortResult - Result of one TCP port check
type DialHostPortResult struct {
	// Ok - true if everything OK
	ConnectOK bool `yaml:"ConnectOK,omitempty"`

	// TCP or UDP
	Protocol string `yaml:"Protocol,omitempty"`

	// Hostname + Port
	HostnamePort string `yaml:"HostnamePort,omitempty"`

	// Error if was
	Error string `yaml:"Error,omitempty"`
}

// AggregateDialHostPortParallel - performs DialHostPortParallel, grabs results
// and returns them as slice
func AggregateDialHostPortParallel(tasks []DialHostPortTask,
	parallelTasks int, printProgress bool) []DialHostPortResult {

	var res = make([]DialHostPortResult, 0, len(tasks))

	for r := range DialHostPortParallel(tasks, parallelTasks, printProgress) {
		res = append(res, r)
	}

	return res
}

// DialHostPortParallel - Performs port check and returns result
func DialHostPortParallel(tasks []DialHostPortTask,
	parallelTasks int, printProgress bool) <-chan DialHostPortResult {
	// Create sender
	var tasksGen = dialHostPortTasksSender(tasks)

	// Create workers
	var workersOut = make([]<-chan DialHostPortResult, parallelTasks)
	for i := 0; i < parallelTasks; i++ {
		workersOut[i] = dialHostPortTasksProcessor(tasksGen, printProgress)
	}

	// Grab result
	return dialHostPortResultGrabber(workersOut)
}

// dialHostPortTasksSender - Sends port check tasks to workers
func dialHostPortTasksSender(tasks []DialHostPortTask) <-chan DialHostPortTask {
	var out = make(chan DialHostPortTask)
	go func() {
		for _, task := range tasks {
			out <- task
		}

		close(out)
	}()

	return out
}

// dialHostPortTasksProcessor - Processes port checks and returns results
func dialHostPortTasksProcessor(
	inChan <-chan DialHostPortTask, printProgress bool) <-chan DialHostPortResult {

	var out = make(chan DialHostPortResult)

	go func() {
		for inTask := range inChan {
			// Process
			connection, err := net.Dial(inTask.Protocol, inTask.HostnamePort)
			defer func() {
				if err == nil {
					connection.Close()
				}
			}()

			if err != nil {
				out <- DialHostPortResult{
					ConnectOK:    false,
					Protocol:     inTask.Protocol,
					HostnamePort: inTask.HostnamePort,
					Error:        err.Error(),
				}
			} else {
				out <- DialHostPortResult{
					ConnectOK:    true,
					Protocol:     inTask.Protocol,
					HostnamePort: inTask.HostnamePort,
				}
			}
			if printProgress {
				if err == nil {
					println(fmt.Sprintf(
						"Succeeded checking connection to host %v; Protocol: %v; StatusOk: %v",
						inTask.HostnamePort, inTask.Protocol, err == nil))
				} else {
					println(fmt.Sprintf(
						"Error checking connection to host %v; Protocol: %v; StatusOk: %v",
						inTask.HostnamePort, inTask.Protocol, err == nil))
				}
			}
		}

		close(out)
	}()

	return out
}

// dialHostPortResultGrabber - Gets results
func dialHostPortResultGrabber(
	resultChannels []<-chan DialHostPortResult) <-chan DialHostPortResult {

	var out = make(chan DialHostPortResult)
	var waitGroup sync.WaitGroup

	// Start merge tasks
	// Create a function which gets results from one task and sends to another
	for _, inResChan := range resultChannels {
		waitGroup.Add(1)

		go func(ch <-chan DialHostPortResult) {
			for val := range ch {
				out <- val
			}

			// Task finished
			waitGroup.Done()
		}(inResChan)
	}

	// Start result waiter
	go func() {
		waitGroup.Wait()
		close(out)
	}()

	return out
}
