package nettest

import (
	"sync"
)

const (
	parallelTasks = 5
)

// TestSet - Set of Test tasks
type TestSet struct {
	LookupHostTasks   []LookupHostTask
	PingTasks         []PingTask
	GetHTTPTasks      []GetHTTPTask
	DialHostPortTasks []DialHostPortTask
	TracerouteTasks   []TracerouteTask
}

// TestResultSet - Set of Results
type TestResultSet struct {
	LookupHostResults   []LookupHostResult   `yaml:"LookupHostResults,omitempty"`
	PingResults         []PingResult         `yaml:"PingResults,omitempty"`
	GetHTTPResults      []GetHTTPResult      `yaml:"GetHTTPResults,omitempty"`
	DialHostPortResults []DialHostPortResult `yaml:"DialHostPortResults,omitempty"`
	TracerouteResults   []TracerouteResult   `yaml:"TracerouteResults,omitempty"`
}

// PerformAllTests - Executes all test tasks and returns full result
//
func PerformAllTests(ts TestSet, printProgress bool) TestResultSet {
	var wg sync.WaitGroup

	var result = TestResultSet{}

	// Set WaitGroup
	wg.Add(5) // 5 types of tests

	// Execute tests
	// Lookup host
	go func() {
		result.LookupHostResults = AggregateLookupHostParallel(
			ts.LookupHostTasks, parallelTasks, printProgress)

		wg.Done()
	}()

	// Ping
	go func() {
		result.PingResults = AggregatePingParallel(ts.PingTasks,
			parallelTasks, printProgress)
		wg.Done()
	}()

	// Get HTTP
	go func() {
		result.GetHTTPResults = AggregateGetHTTPParallel(ts.GetHTTPTasks,
			parallelTasks, printProgress)

		wg.Done()
	}()

	// Check port
	go func() {
		result.DialHostPortResults = AggregateDialHostPortParallel(
			ts.DialHostPortTasks, parallelTasks, printProgress)

		wg.Done()
	}()

	// Traceroute
	go func() {
		result.TracerouteResults = AggregateTracerouteParallel(
			ts.TracerouteTasks, parallelTasks, printProgress)

		wg.Done()
	}()

	// Wait
	wg.Wait()

	return result
}
