package nettest

import (
	"fmt"
	"sync"
	"time"

	"github.com/kanocz/tracelib"
)

// TracerouteTask - aaa
type TracerouteTask struct {
	// Destination to traceroute to
	Destination string

	// Maximum Round Trip time before Timeout
	MaxRTTMS int

	// Time To Live
	MaxTTL int

	// How many times perform traceroute to Destination
	TriesCount int
}

// TracerouteResult - aaa
type TracerouteResult struct {
	Aggregated  [][]tracelib.MHop `yaml:"AggregatedStatistics,omitempty"`
	Path        [][]tracelib.Hop  `yaml:"Path,omitempty"`
	Destination string            `yaml:"Destination,omitempty"`
	Error       string            `yaml:"Error,omitempty"`
}

// AggregateTracerouteParallel - performs TracerouteParallel, grabs results
// and returns them as slice
func AggregateTracerouteParallel(tasks []TracerouteTask,
	parallelTasks int, printProgress bool) []TracerouteResult {

	var res = make([]TracerouteResult, 0, len(tasks))

	for r := range TracerouteParallel(tasks, parallelTasks, printProgress) {
		res = append(res, r)
	}

	return res
}

// TracerouteParallel -aserd
func TracerouteParallel(tasks []TracerouteTask,
	parallelTasks int, printProgress bool) <-chan TracerouteResult {

	// Create sender
	var tasksGen = tracerouteTasksSender(tasks)

	// Create workers
	var workersOut = make([]<-chan TracerouteResult, parallelTasks)
	for i := 0; i < parallelTasks; i++ {
		workersOut[i] = tracerouteTasksProcessor(tasksGen, printProgress)
	}

	// Grab result
	return tracertResultGrabber(workersOut)
}

func tracerouteTasksSender(tasks []TracerouteTask) <-chan TracerouteTask {
	var out = make(chan TracerouteTask)
	go func() {
		for _, task := range tasks {
			out <- task
		}

		close(out)
	}()

	return out
}

func tracerouteTasksProcessor(inChan <-chan TracerouteTask,
	printProgress bool) <-chan TracerouteResult {

	var out = make(chan TracerouteResult)

	go func() {

		DNSCache := tracelib.NewLookupCache()

		for inTask := range inChan {
			// Process
			mhop, err := tracelib.RunMultiTrace(inTask.Destination,
				"0.0.0.0", "::",
				time.Duration(inTask.MaxRTTMS)*time.Millisecond,
				inTask.MaxTTL,
				DNSCache,
				inTask.TriesCount, nil)

			if err != nil {
				out <- TracerouteResult{Error: err.Error()}

				// Report err
				if printProgress {
					fmt.Printf("Error Traceroute to host %v. %v",
						inTask.Destination, err)
				}

				// Skip remained steps
				continue
			}

			// Get last hop in the result
			// If there is not at least one "final" hop - set error
			lastHops := mhop[len(mhop)-1]
			var foundLast bool
			for _, hop := range lastHops {
				if hop.Final {
					foundLast = true

					break
				}
			}

			// Set error if tested host was not reached
			var errorMsg string
			if !foundLast {
				errorMsg = fmt.Sprintf("Error Traceroute: Host %v was not reached",
					inTask.Destination)
			}

			// If traceroute OK - build result and include the information
			// about destination host reachability
			out <- TracerouteResult{
				Destination: inTask.Destination,
				Error:       errorMsg,
				Aggregated:  tracelib.AggregateMulti(mhop),
				// Path:       mhop,
			}

			if printProgress {
				if foundLast {
					println(fmt.Sprintf(
						"Succeeded Traceroute to host %v", inTask.Destination))
				} else {
					println(errorMsg)
				}
			}

		}

		close(out)
	}()

	return out
}

func tracertResultGrabber(
	resultChannels []<-chan TracerouteResult) <-chan TracerouteResult {

	var out = make(chan TracerouteResult)
	var waitGroup sync.WaitGroup

	// Start merge tasks
	// Create a function which gets results from one task and sends to another
	for _, inResChan := range resultChannels {
		waitGroup.Add(1)

		go func(ch <-chan TracerouteResult) {
			for val := range ch {
				out <- val
			}

			// Task finished
			waitGroup.Done()
		}(inResChan)
	}

	// Start result waiter
	go func() {
		waitGroup.Wait()
		close(out)
	}()

	return out
}
