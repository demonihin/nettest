
help:
	@echo "Execute required subcommand to build the application for required architecture"
	@echo "Commands are: build-native, build-linux-i386, build-linux-amd64, build-windows-i386, build-windows-amd64"

build-native:
	mkdir -p build/
	go build -o build/NetTest_native

build-linux-i386:
	mkdir -p build/	
	GOOS="linux" GOHOSTARCH="i386" go build -o build/NetTest_linux_i386

build-linux-amd64:
	mkdir -p build/
	GOOS="linux" GOHOSTARCH="i386" go build -o build/NetTest_linux_amd64

build-windows-i386:
	mkdir -p build/
	GOOS="windows" GOHOSTARCH="i386" go build -o build/NetTest_windows_i386.exe

build-windows-amd64:
	mkdir -p build/
	GOOS="windows" GOHOSTARCH="amd64" go build -o build/NetTest_windows_amd64.exe

build-all: build-linux-i386 build-windows-i386 build-linux-amd64 build-windows-amd64
