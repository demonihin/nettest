package main

import (
	"io"
	"log"
	"os"
	"time"

	"gitlab.com/demonihin/NetTest/nettest"
	yaml "gopkg.in/yaml.v2"
)

func main() {

	var tasks = nettest.TestSet{
		DialHostPortTasks: []nettest.DialHostPortTask{
			nettest.DialHostPortTask{
				HostnamePort: "ya.ru:80",
				Protocol:     nettest.DialProtocolTCP,
			},
			nettest.DialHostPortTask{
				HostnamePort: "google.com.ru:80",
				Protocol:     nettest.DialProtocolTCP,
			},
		},
		GetHTTPTasks: []nettest.GetHTTPTask{
			nettest.GetHTTPTask{
				URL:         "http://mail.ru",
				DetectProxy: true,
			},
			nettest.GetHTTPTask{
				URL: "http://gmail.com",
			},
		},
		LookupHostTasks: []nettest.LookupHostTask{
			nettest.LookupHostTask{HostnameToResolve: "aol.com"},
			nettest.LookupHostTask{HostnameToResolve: "microsoft.com"},
		},
		PingTasks: []nettest.PingTask{
			nettest.PingTask{
				Destination:  "ya.ru",
				IntervalMS:   1000,
				PacketSize:   32,
				PacketsCount: 5,
			},
			nettest.PingTask{
				Destination:  "8.8.8.8",
				IntervalMS:   1000,
				PacketSize:   32,
				PacketsCount: 5,
			},
		},
		TracerouteTasks: []nettest.TracerouteTask{
			nettest.TracerouteTask{
				Destination: "ya.ru",
				MaxRTTMS:    500,
				MaxTTL:      30,
				TriesCount:  3,
			},
			nettest.TracerouteTask{
				Destination: "google.com",
				MaxRTTMS:    500,
				MaxTTL:      30,
				TriesCount:  3,
			},
		},
	}

	// Execute tests
	resultSet := nettest.PerformAllTests(tasks, true)

	// Make report
	data, err := yaml.Marshal(resultSet)
	if err != nil {
		log.Fatalln("Can not Marshall result")
	}

	// Make file and open it for writing
	hostname, getHostnameerr := os.Hostname()
	var reportOut io.StringWriter
	if getHostnameerr != nil {
		println(
			`Can not get os.Hostname to build report filename. 
				Print result to the screen`)
		reportOut = os.Stdout
	} else {
		fileName := (hostname + "-" +
			time.Now().Format("20060102_150405_-0700_MST") + ".yml")

		file, err := os.OpenFile(fileName,
			os.O_WRONLY|os.O_TRUNC|os.O_CREATE, 0777)
		if err != nil {
			println(
				`Can not open output filename for writing. 
					Print result to the screen`)
			reportOut = os.Stdout
		} else {
			reportOut = file
		}
	}

	_, err = reportOut.WriteString("---\n")
	if err != nil {
		log.Fatalln(err)
	}
	_, err = reportOut.WriteString(string(data))
	if err != nil {
		log.Fatalln(err)
	}
}
