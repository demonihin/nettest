# Nettest

A small utility which helps first-line support staff members(or even end users)
in testing network on end PCs.

The utility is intended to contain hard-coded set of tests which must be
tuned to every network.
The possible tests are:

- ICMP ping
- DNS lookup
- TCP and UDP port open check
- Traceroute
- HTTP Get of resource

The unitily reports progress in short messages and in the end creates
YAML formatted report in a file current directory.

The file's name has format: hostname-current timestamp.yml

P.S.:
I have found out that Windows 10 blocks incoming ICMP Time to live exceeded packets by default.
To make the utility work with Windows 10 you have to add a new rule to Windows 10 Firewall with advanced security:

For IPv6:
`netsh advfirewall firewall add rule name="Allow ICMPv6 TTL In" protocol=ICMPv6:11,any dir=in action=allow`

For IPv4:
`netsh advfirewall firewall add rule name="Allow ICMPv4 TTL In" protocol=ICMPv4:11,any dir=in action=allow`
