module gitlab.com/demonihin/NetTest

go 1.12

require (
	gitlab.com/demonihin/NetTest/nettest v0.0.0-00010101000000-000000000000
	gopkg.in/yaml.v2 v2.2.2
)

replace gitlab.com/demonihin/NetTest/nettest => ./nettest
